module.exports = {
  /*
  ** Подключение глобальнх переменных
  */
  // css: ['./assets/sass/main.sass'],
  // styleResources: {
  //   sass: [
  //     '~/assets/sass/*.sass'
  //     ]
  // },
  modules: [
    '@nuxtjs/style-resources',
    '@nuxtjs/axios',],
  
  styleResources: {
    sass: [
      '~assets/sass/_params.sass'
    ]
  },
  css: [
    // модуль node.js, но с указанием пре-процессора
    { src: '~assets/sass/main.sass', lang: 'sass' },
    // // Загрузить модуль node.js
    // 'hover.css/css/hover-min.css',
    // // CSS-файл в проекте
    // '~assets/css/main.css'
    'swiper/dist/css/swiper.css'
  ],
  /*
  ** Headers of the page
  */
  head: {
    title: 'odnoinogoi',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  router: {
    middleware: 'changePage'
  },
  plugins: [
    { src: '~/plugins/swiper.js', ssr: false },
  ],
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** собирать CSS отдельно
    */
    extractCSS: true,
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  generate: {
    minify: false
  }
}

