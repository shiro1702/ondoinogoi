import Vue from 'vue'

export const state = () => ({
    menuOpenState: false,
})
export const mutations = {
    menuToogle(state){
        state.menuOpenState = !state.menuOpenState;
    }
}

export const actions = {
    LoadAbout(data){
        return new Promise((resolve, reject) => {
            Vue.http.get(data.url)
                .then(response => response.json())
                .then(
                    data => {
                        console.log(data)
                        resolve(data);
                    },
                    data => {
                        reject(data);
                    }
                )
        })
    }
}