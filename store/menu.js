export const state = () => ({
  items: [
    {
      name: 'index',
      text: 'Главная'
    },
    {
      name: 'videos',
      text: 'Видео'
    },
    {
      name: 'about',
      text: 'О проекте'
    },
    {
      name: 'contacts',
      text: 'Контакты'
    },
  ],
  email: 'abuse@odnojnogoj.ru',
  instagram: '#', 
  menuOpenState: false,
})
export const mutations = {
  // addItems (state, items) {
  //   state.items = item
  // },
  menuToogle(state){
    state.menuOpenState = !state.menuOpenState;
  },
  menuClose(state){
    state.menuOpenState = false;
  }
}
// export const actions = {
//   LoadItems (state, items) {

//     state.items = item
//   },
// }