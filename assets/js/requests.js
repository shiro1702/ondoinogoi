// import Vue from 'vue'
import axios from 'axios'

export function LoadJson(url){
    return new Promise((resolve, reject) => {
        if (process.server) {
            // берем json файл из файловой стситемы
            let fs = require('fs');
            let res = JSON.parse(fs.readFileSync('./static/' + url, 'utf8'));
            resolve(res)
        } else {
            // делаем запрос к json файлу из браузера
            return axios.get(url)
                .then((res) => {
                    resolve(res.data)
                })
        }
    })
}
